#!/bin/bash

cp $HOME/.config/nvim/init.lua nvim/init.lua
cp $HOME/.config/xmobar/xmobar.config xmobar/xmobar.config
cp $HOME/.xmonad/*.hs .xmonad/
cp $HOME/.xmonad/xmonad-session-rc .xmonad/
cp $HOME/.xmonad/*.sh .xmonad/
